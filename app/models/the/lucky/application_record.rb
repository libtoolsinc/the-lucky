module The
  module Lucky
    class ApplicationRecord < ActiveRecord::Base
      self.abstract_class = true
    end
  end
end
