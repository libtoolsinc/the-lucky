module The
  module Lucky
    class Engine < ::Rails::Engine
      isolate_namespace The::Lucky
    end
  end
end
