# The::Lucky
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'the-lucky'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install the-lucky
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
